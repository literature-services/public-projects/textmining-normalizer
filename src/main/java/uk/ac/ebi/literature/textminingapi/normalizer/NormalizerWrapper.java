package uk.ac.ebi.literature.textminingapi.normalizer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import uk.ac.ebi.literature.textminingapi.pojo.AccessionNumber;
import uk.ac.ebi.literature.textminingapi.pojo.BaseAnnotation;
import uk.ac.ebi.literature.textminingapi.pojo.Chemicals;
import uk.ac.ebi.literature.textminingapi.pojo.Diseases;
import uk.ac.ebi.literature.textminingapi.pojo.ExperimentalMethods;
import uk.ac.ebi.literature.textminingapi.pojo.GeneOntology;
import uk.ac.ebi.literature.textminingapi.pojo.GeneProteins;
import uk.ac.ebi.literature.textminingapi.pojo.Organisms;
@Service
public class NormalizerWrapper {

	private Normalizer normalizer;

	private Map<Class<? extends BaseAnnotation>, String> patternMap;

	@Autowired
	public NormalizerWrapper(@Qualifier("baseNormalizer") Normalizer normalizer) {
		this.normalizer = normalizer;
		patternMap = new HashMap<>();
		this.initialize();
	}

	public Map<String, List<BaseAnnotation>> normalize(String content) {
		Map<String, List<BaseAnnotation>> annotationsMap = new HashMap<>();
		for (var clazz : patternMap.keySet()) {
			String pattern = patternMap.get(clazz);
			List<BaseAnnotation> collection = normalizer.normalize(content, pattern, clazz);
			if (collection.isEmpty())
				continue;
			String key = clazz.getSimpleName();
			annotationsMap.putIfAbsent(key, new LinkedList<>());
			annotationsMap.get(key).addAll(collection);
		}
		return annotationsMap;
	}

	private void initialize() {
		patternMap.put(AccessionNumber.class, "<z:acc[\\s\\S]*?</z:acc>");
		patternMap.put(GeneProteins.class, "<z:uniprot[\\s\\S]*?</z:uniprot>");
		patternMap.put(Organisms.class, "<z:species[\\s\\S]*?</z:species>");
		patternMap.put(Chemicals.class, "<z:chebi[\\s\\S]*?</z:chebi>");
		patternMap.put(GeneOntology.class, "<z:go[\\s\\S]*?</z:go>");
		patternMap.put(Diseases.class, "<z:disease[\\s\\S]*?</z:disease>");
		patternMap.put(ExperimentalMethods.class, "<z:methods[\\s\\S]*?</z:methods>");
	}
}
