package uk.ac.ebi.literature.textminingapi.pojo;

import java.util.HashMap;
import java.util.Map;

/**
 * <z:acc db="arrayexpress" valmethod="onlineWithContext" domain=
 * "geneExpression" context="(?i)(arrayexpress|atlas|gxa|accession|experiment)"
 * wsize="200" sec="REF">E-MTAB-10886</z:acc>
 * 
 * boolean field resources is true when domain attribute is empty/null else
 * false
 * 
 * @author ssharma
 *
 */
public class AccessionNumber extends BaseAnnotation {

	private String db;

	private String valmethod;

	private String domain;

	private String context;

	private String wsize;

	private String sec;

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getValmethod() {
		return valmethod;
	}

	public void setValmethod(String valmethod) {
		this.valmethod = valmethod;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getWsize() {
		return wsize;
	}

	public void setWsize(String wsize) {
		this.wsize = wsize;
	}

	public String getSec() {
		return sec;
	}

	public void setSec(String sec) {
		this.sec = sec;
	}

	public boolean isResources() {
		return resourceURLMap.containsKey(db) ? true : false;
	}

	@Override
	public String getType() {
		return isResources() ? "Resources" : "Accession Numbers";
	}

	/**
	 * <p>
	 * <b>AccessionNumber</b> : The Url mapping is populated using the db attribute
	 * and the content inside the tag
	 * </p>
	 * <p>
	 * <b>Resources</b> : The Url mapping is populated using the db and id field
	 * attributes together with the value
	 * </p>
	 * 
	 */
	@Override
	public String getUrl() {
		if (this.isResources()) {
			return new StringBuilder(resourceURLMap.getOrDefault(this.getDb(), this.getDb() + " : UNKOWN")).toString();
		} else {
			String accDb = idMap.get(this.getDb());
			if (specialAccDb.indexOf(accDb) > -1) {
				StringBuilder uri = new StringBuilder();
				if ("empiar".equals(accDb)) {
					String value = this.getValue().replace("EMPIAR-", "");
					uri.append("https://www.ebi.ac.uk/pdbe/emdb/empiar/entry/").append(value);
				} else if ("hipsci".equals(accDb)) {
					uri.append("http://www.hipsci.org/lines/#/lines/").append(this.getValue());
				} else if ("ebisc".equals(accDb)) {
					uri.append("https://cells.ebisc.org/").append(this.getValue());
				} else if ("eva".equals(accDb)) {
					uri.append("https://www.ncbi.nlm.nih.gov/snp/?term=").append(this.getValue());
				} else if ("hpa".equals(accDb)) {
					uri.append("https://www.proteinatlas.org/search/").append(this.getValue());
				} else if ("ega.dac".equals(accDb)) {
					uri.append("https://www.ebi.ac.uk/ega/dacs/").append(this.getValue());
				} else if ("orphadata".equals(accDb)) {
					String value = this.getValue().replace("ORPHA:", "").replace("ORPHA", "");
					uri.append("http://identifiers.org/orphanet:").append(value);
				} else if ("rrid".equals(accDb)) {
					uri.append("http://identifiers.org/").append(this.getValue());
				} else if ("pxd".equals(accDb)) {
					uri.append("http://identifiers.org/px:").append(this.getValue());
				} else if ("efo".equals(accDb)) {
					uri.append("http://identifiers.org/").append(this.getValue());
				} else if ("go".equals(accDb)) {
					uri.append("http://identifiers.org/").append(this.getValue());
				} else if ("omim".equals(accDb)) {
					uri.append("https://omim.org/entry/").append(this.getValue());
				} else if ("gisaid".equals(accDb)) {
					String value = this.getValue();
					if (value.startsWith("EPI_ISL")) {
						value = value.substring(value.indexOf("EPI_ISL") + 7);
						uri.append("http://gisaid.org/EPI_ISL/").append(value);
					} else if (value.startsWith("EPI")) {
						value = value.substring(value.indexOf("EPI") + 3);
						uri.append("http://gisaid.org/EPI/").append(value);
					}
				} else {
					return "acc db " + accDb + " should be mapped NEW!\n";
				}
				return uri.toString();
			} else {
				return new StringBuilder("http://identifiers.org/").append(accDb).append(":").append(this.getValue())
						.toString();
			}
		}
	}

	public final static Map<String, String> resourceURLMap = new HashMap<>();
	public final static Map<String, String> idMap = new HashMap<>();
	public final static String specialAccDb = "(hpa|hipsci|ebisc|eva|empiar|ega.dac|orphadata|pxd|rrid|efo|go|omim|gisaid)";

	static {
		resourceURLMap.put("Runiprot", "http://identifiers.org/miriam.resource:MIR:00100164");
		resourceURLMap.put("Rpdb", "http://identifiers.org/miriam.resource:MIR:00100037");
		resourceURLMap.put("Rwormbase", "http://identifiers.org/miriam.resource:MIR:00100038");
		resourceURLMap.put("Rvectorbase", "http://identifiers.org/miriam.resource:MIR:00100291");
		resourceURLMap.put("Rsurechembl", "http://www.surechembl.org");
		resourceURLMap.put("Rrnacentral", "http://identifiers.org/miriam.resource:MIR:00100834");
		resourceURLMap.put("Rrfam", "http://identifiers.org/miriam.resource:MIR:00100686");
		resourceURLMap.put("Rreactome", "http://identifiers.org/miriam.resource:MIR:00100026");
		resourceURLMap.put("Rpride", "http://identifiers.org/miriam.resource:MIR:00100094");
		resourceURLMap.put("Rpfam", "http://identifiers.org/miriam.resource:MIR:00100685");
		resourceURLMap.put("Rols", "http://www.ebi.ac.uk/ols");
		resourceURLMap.put("Rmetabolights", "http://identifiers.org/miriam.resource:MIR:00100486");
		resourceURLMap.put("Rinterpro", "http://identifiers.org/miriam.resource:MIR:00100018");
		resourceURLMap.put("Rintenz", "http://identifiers.org/miriam.resource:MIR:00100001");
		resourceURLMap.put("Rintact", "http://identifiers.org/miriam.resource:MIR:00100017");
		resourceURLMap.put("Rigsr", "http://www.internationalgenome.org");
		resourceURLMap.put("Ridentifiers", "http://identifiers.org/miriam.resource:MIR:00100800");
		resourceURLMap.put("Rhgnc", "http://identifiers.org/miriam.resource:MIR:00100460");
		resourceURLMap.put("Rgwas", "http://www.ebi.ac.uk/gwas");
		resourceURLMap.put("Rgo", "http://identifiers.org/miriam.resource:MIR:00100013");
		resourceURLMap.put("Rgxa", "http://identifiers.org/miriam.resource:MIR:00100482");
		resourceURLMap.put("Reva", "http://www.ebi.ac.uk/eva");
		resourceURLMap.put("Repmc", "http://identifiers.org/miriam.resource:MIR:00100497");
		resourceURLMap.put("Renzymeportal", "http://identifiers.org/miriam.resource:MIR:00100835");
		resourceURLMap.put("Rensemblgenomes", "http://ensemblgenomes.org");
		resourceURLMap.put("Rensembl", "http://identifiers.org/miriam.resource:MIR:00100011");
		resourceURLMap.put("Rena", "http://identifiers.org/miriam.resource:MIR:00100473");
		resourceURLMap.put("Rempiar", "http://www.ebi.ac.uk/pdbe/emdb/empiar");
		resourceURLMap.put("Remdb", "http://identifiers.org/miriam.resource:MIR:00100738");
		resourceURLMap.put("Rega", "http://identifiers.org/miriam.resource:MIR:00100658");
		resourceURLMap.put("Refo", "http://identifiers.org/miriam.resource:MIR:00100511");
		resourceURLMap.put("Rmetagenomics", "http://identifiers.org/miriam.resource:MIR:00100656");
		resourceURLMap.put("Rcomplexportal", "http://identifiers.org/miriam.resource:MIR:00100875");
		resourceURLMap.put("Rchembl", "http://identifiers.org/miriam.resource:MIR:00100115");
		resourceURLMap.put("Rchebi", "http://identifiers.org/miriam.resource:MIR:00100009");
		resourceURLMap.put("Rbiostudies", "http://www.ebi.ac.uk/biostudies");
		resourceURLMap.put("Rbiosamples", "http://identifiers.org/miriam.resource:MIR:00100447");
		resourceURLMap.put("Rbiomodels", "http://identifiers.org/miriam.resource:MIR:00100006");
		resourceURLMap.put("Rarrayexpress", "http://identifiers.org/miriam.resource:MIR:00100061");
		resourceURLMap.put("Rstringdb", "http://identifiers.org/miriam.resource:MIR:00100342");
		resourceURLMap.put("Rmint", "http://identifiers.org/miriam.resource:MIR:00100654");
		resourceURLMap.put("Rcath", "http://identifiers.org/miriam.resource:MIR:00100816");
		resourceURLMap.put("Rhpa", "http://identifiers.org/miriam.resource:MIR:00100431");
		resourceURLMap.put("Rorphadata", "http://www.orphadata.org");
		resourceURLMap.put("Rsilva", "https://www.arb-silva.de");
		resourceURLMap.put("Rbrenda", "http://identifiers.org/miriam.collection:MIR:00000071");
		resourceURLMap.put("Runichem", "http://www.ebi.ac.uk/unichem");
		resourceURLMap.put("Remma", "http://www.infrafrontier.eu");

		idMap.put("arrayexpress", "ebi/arrayexpress"); // ebi
		idMap.put("biomodels", "ebi/biomodels.db"); // ebi
		idMap.put("biosample", "ebi/biosample"); // ebi
		idMap.put("biostudies", "biostudies");
		idMap.put("chebi", "ebi/chebi"); // ebi
		idMap.put("chembl", "ebi/chembl.compound"); // ebi
		// complex portal
		idMap.put("complexportal", "complexportal");
		// ebi metagenomics
		idMap.put("metagenomics", "ebi/ena.embl"); // ebi
		idMap.put("efo", "efo"); // $specialAccDbs
		// idMap.put("ega","ega.study");
		idMap.put("emdb", "emdb");
		idMap.put("empiar", "empiar"); // $specialAccDbs
		idMap.put("gen", "ebi/ena.embl"); // ebi
		idMap.put("ensembl", "ensembl");
		// enzyme portal
		// epmc
		idMap.put("eva", "eva"); // Needs investigation
		// expression atlas
		idMap.put("go", "go"); // $specialAccDbs
		// gwas catalogue
		idMap.put("hgnc", "hgnc");
		idMap.put("igsr", "coriell");
		idMap.put("1000genomes", "coriell");
		idMap.put("intact", "intact");
		// intenz
		idMap.put("interpro", "ebi/interpro"); // ebi
		idMap.put("metabolights", "ebi/metabolights"); // ebi
		// mouse resources
		// ols
		idMap.put("pdb", "pdbe/pdb"); // pdbe
		idMap.put("pfam", "pfam");
		idMap.put("pxd", "pxd");// $specialAccDbs
		idMap.put("reactome", "reactome");
		idMap.put("rfam", "rfam");
		idMap.put("rnacentral", "rnacentral");
		// surechembl
		idMap.put("sprot", "uniprot");
		idMap.put("uniprot", "uniprot");
		// vectorbase
		// wormbase

		idMap.put("AB", "rrid"); // $specialAccDbs
		idMap.put("CVCL", "rrid"); // $specialAccDbs
		idMap.put("SCR", "rrid"); // $specialAccDbs
		idMap.put("rrid", "rrid"); // $specialAccDbs
		idMap.put("refsnp", "dbsnp"); // http://www.ncbi.nlm.nih.gov/SNP/snp_ref.cgi?rs=rs4686484
		idMap.put("nct", "clinicaltrials");
		idMap.put("refseq", "refseq");
		idMap.put("omim", "omim"); // $specialAccDbs
		idMap.put("doi", "doi");
		idMap.put("bioproject", "ebi/bioproject"); // ebi
		idMap.put("eudract", "euclinicaltrials");
		idMap.put("treefam", "treefam");
		idMap.put("ebisc", "ebisc"); // $specialAccDbs
		idMap.put("hipsci", "hipsci"); // $specialAccDbs
		idMap.put("gca", "insdc.gca");
		idMap.put("ega", "ega.dataset");
		idMap.put("cath", "cath");
		idMap.put("hpa", "hpa"); // $specialAccDbs
		idMap.put("mint", "mint");
		idMap.put("emma", "emma");
		idMap.put("dbgap", "dbgap");
		idMap.put("geo", "geo");
		idMap.put("uniparc", "uniparc");
		idMap.put("ega.study", "ebi/ega.study"); // ebi
		idMap.put("ega.dataset", "ebi/ega.dataset"); // ebi
		idMap.put("ega.dac", "ega.dac"); // # $specialAccDbs
		idMap.put("orphadata", "orphadata"); // # $specialAccDbs
		idMap.put("gisaid", "gisaid"); // $specialAccDbs
	}

}
