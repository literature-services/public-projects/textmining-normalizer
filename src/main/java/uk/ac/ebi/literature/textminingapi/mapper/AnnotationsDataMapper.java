package uk.ac.ebi.literature.textminingapi.mapper;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ebi.literature.textminingapi.pojo.AnnotationInfo;
import uk.ac.ebi.literature.textminingapi.pojo.AnnotationTag;
import uk.ac.ebi.literature.textminingapi.pojo.AnnotationsData;
import uk.ac.ebi.literature.textminingapi.pojo.BaseAnnotation;

@Component
@Qualifier("annotationsDataMapper")
public class AnnotationsDataMapper implements Mapper<Map<String, List<BaseAnnotation>>, AnnotationsData> {

	@Override
	public AnnotationsData map(Map<String, List<BaseAnnotation>> annotationsMap) {

		Map<String, List<BaseAnnotation>> groupedAnnotations = annotationsMap.values().stream()
				.flatMap(list -> list.stream())
				.collect(Collectors.groupingBy(ann -> (ann.getType() + "_" + ann.getValue()), Collectors.toList()));

		AnnotationsData annotationsData = new AnnotationsData();
		List<AnnotationInfo> annotationsInfo = new LinkedList<>();
		for (String key : groupedAnnotations.keySet()) {

			List<BaseAnnotation> collection = groupedAnnotations.get(key);
			BaseAnnotation annotation = collection.get(0);

			AnnotationInfo annotationInfo = getAnnotationInfo(annotation, collection.size());

			annotationsInfo.add(annotationInfo);

		}
		annotationsData.setAnns(toAnnotationsInfoArray(annotationsInfo));

		return annotationsData;
	}

	private final AnnotationInfo getAnnotationInfo(BaseAnnotation baseAnnotation, int freq) {
		AnnotationInfo annotationInfo = new AnnotationInfo();
		annotationInfo.setExact(baseAnnotation.getValue());
		annotationInfo.setFrequency(freq);
		annotationInfo.setType(baseAnnotation.getType());

		annotationInfo.setTags(getAnnotationTags(baseAnnotation));

		return annotationInfo;
	}

	private final AnnotationTag[] getAnnotationTags(BaseAnnotation baseAnnotation) {
		AnnotationTag annotationTag = new AnnotationTag();
		annotationTag.setName(baseAnnotation.getValue());
		annotationTag.setUri(baseAnnotation.getUrl());

		return new AnnotationTag[] { annotationTag };
	}

	private final AnnotationInfo[] toAnnotationsInfoArray(List<AnnotationInfo> annotationsInfo) {
		AnnotationInfo[] annotationsInfoArray = new AnnotationInfo[annotationsInfo.size()];
		for (int i = 0; i < annotationsInfo.size(); i++) {
			annotationsInfoArray[i] = annotationsInfo.get(i);
		}
		return annotationsInfoArray;
	}

}
