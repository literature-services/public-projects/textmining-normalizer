package uk.ac.ebi.literature.textminingapi.pojo;

/**
 * <z:chebi fb="0" ids="10003" onto="chemical_enitity">vistamycin</z:chebi>
 * 
 * @author ssharma
 *
 */
public class Chemicals extends BaseAnnotation {

	private String fb;
	private String ids;
	private String onto;

	public String getFb() {
		return fb;
	}

	public void setFb(String fb) {
		this.fb = fb;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getOnto() {
		return onto;
	}

	public void setOnto(String onto) {
		this.onto = onto;
	}

	@Override
	public String getType() {
		return "Chemicals";
	}

	@Override
	public String getUrl() {
		return new StringBuilder("http://purl.obolibrary.org/obo/CHEBI_").append(getId(this.ids)).toString();
	}

}
