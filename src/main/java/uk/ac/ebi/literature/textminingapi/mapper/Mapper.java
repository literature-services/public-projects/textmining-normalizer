package uk.ac.ebi.literature.textminingapi.mapper;

@FunctionalInterface
public interface Mapper<I, O> {

	O map(I i);

}
