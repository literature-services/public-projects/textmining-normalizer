package uk.ac.ebi.literature.textminingapi.normalizer;

import java.util.List;

import uk.ac.ebi.literature.textminingapi.pojo.BaseAnnotation;

public interface Normalizer {

	List<BaseAnnotation> normalize(String pattern, String content, Class<? extends BaseAnnotation> clazz);

}
