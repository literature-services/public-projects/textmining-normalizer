package uk.ac.ebi.literature.textminingapi;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import uk.ac.ebi.literature.textminingapi.mapper.Mapper;
import uk.ac.ebi.literature.textminingapi.normalizer.NormalizerWrapper;
import uk.ac.ebi.literature.textminingapi.pojo.AnnotationsData;
import uk.ac.ebi.literature.textminingapi.pojo.BaseAnnotation;
import uk.ac.ebi.literature.textminingapi.pojo.Components;
import uk.ac.ebi.literature.textminingapi.pojo.MLTextObject;
import uk.ac.ebi.literature.textminingapi.service.FileService;
import uk.ac.ebi.literature.textminingapi.service.MLQueueSenderService;
import uk.ac.ebi.literature.textminingapi.utility.Utility;

@Service
public class TextminingApiNormalizerService {

	private static final Logger log = LoggerFactory.getLogger(TextminingApiNormalizerApplication.class);

	@Value("${rabbitmq.tmExchange}")
	private String textminingExchange;

	@Value("${rabbitmq.jsonForApiQueue}")
	private String textminingJsonForApiQueue;

	@Value("${rabbitmq.outcomeQueue}")
	private String textminingOutcomeQueue;

	private final MLQueueSenderService mlQueueSenderService;

	private final FileService fileService;

	private final NormalizerWrapper normalizerWrapper;

	private final Mapper<Map<String, List<BaseAnnotation>>, AnnotationsData> annotationsDataMapper;

	public TextminingApiNormalizerService(MLQueueSenderService mlQueueSenderService, FileService fileService, NormalizerWrapper normalizerWrapper, @Qualifier("annotationsDataMapper") Mapper<Map<String, List<BaseAnnotation>>, AnnotationsData> annotationsDataMapper) {
		this.mlQueueSenderService = mlQueueSenderService;
		this.fileService = fileService;
		this.normalizerWrapper = normalizerWrapper;
		this.annotationsDataMapper = annotationsDataMapper;
	}


	@RabbitListener(autoStartup = "${rabbitmq.rawAnnotationQueue.autoStartUp}", queues = "${rabbitmq.rawAnnotationQueue}")
	public void listenForMessage(Message message) throws Exception {
		var mlTextObject = Utility.castMessage(message, MLTextObject.class);

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Message arrived with Status {"
				+ mlTextObject.getStatus() + "}");

		if (mlQueueSenderService.hasExceededRetryCount(message)) {
			log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} retry count exceeded");
			Utility.markMessageAsFailed(mlTextObject, Components.NORMALIZER);
			this.mlQueueSenderService.sendMessageToQueue(textminingOutcomeQueue, mlTextObject, textminingExchange);
			
			boolean deleted = fileService.delete(mlTextObject.getProcessingFilename());
			log.info("{" + mlTextObject.getFtId() + "," + mlTextObject.getUser() + "} Deleted "
					+ mlTextObject.getProcessingFilename() + "? " + deleted);
			
			return;
		}

		String processingFileName = mlTextObject.getProcessingFilename();

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Reading file {" + processingFileName
				+ "} from storage");

		byte[] fileContentBytes = this.fileService.read(processingFileName);

		String fileContentStr = new String(fileContentBytes);

		var annotationsMap = this.normalizerWrapper.normalize(fileContentStr);

		if(annotationsMap.isEmpty())
			throw new Exception("No Annotations Found in Raw Annotations Text File "+processingFileName);
		
		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Annotations From File with size {"
				+ annotationsMap.size() + "}");

		var annotationsData = this.annotationsDataMapper.map(annotationsMap);
		annotationsData.setFtId(mlTextObject.getFtId());
		annotationsData.setUser(mlTextObject.getUser());
		annotationsData.setFilename(mlTextObject.getFilename());

		String annotationsDataJsonStr = Utility.asJsonString(annotationsData);

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Serialized {"
				+ annotationsDataJsonStr + "}");

		String fileName = new StringBuilder(mlTextObject.getUser()).append("_").append(mlTextObject.getFtId())
				.append("_").append(mlTextObject.getFilename()).append("_json_for_api.txt").toString();

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Writing file {" + fileName
				+ "} to storage");
		this.fileService.write(annotationsDataJsonStr, fileName);

		mlTextObject.setProcessingFilename(fileName);

		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Sending to Queue");

		boolean sending = this.mlQueueSenderService.sendMessageToQueue(textminingJsonForApiQueue, mlTextObject,
				textminingExchange);

		if (!sending) {
			log.error("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Sending to Queue Failed");
			throw new Exception("Impossible to store Success in JsonForApiQueue for " + mlTextObject.toString());
		}
		log.info("{" + mlTextObject.getFtId() + ", " + mlTextObject.getUser() + "} Deleting raw annotations file {"
				+ processingFileName + "} from storage");

		this.fileService.delete(processingFileName);
	}
}
