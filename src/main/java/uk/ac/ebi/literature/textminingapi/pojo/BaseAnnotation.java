package uk.ac.ebi.literature.textminingapi.pojo;

public abstract class BaseAnnotation {

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	protected String getId(String ids) {
		var idArr = ids.split(",");
		if (idArr.length == 0)
			return "";
		return idArr[0];
	}

	public abstract String getType();

	public abstract String getUrl();

}
