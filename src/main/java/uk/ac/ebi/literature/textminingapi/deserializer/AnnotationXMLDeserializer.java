package uk.ac.ebi.literature.textminingapi.deserializer;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@Service
@Qualifier("annotationXMLDeserializer")
public class AnnotationXMLDeserializer<BaseAnnotation> implements Deserializer<BaseAnnotation> {

	private static final Logger log = LoggerFactory.getLogger(AnnotationXMLDeserializer.class);

	private DocumentBuilder builder;

	public AnnotationXMLDeserializer() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			log.error("Initialization Exception", e);
		}
	}

	@Override
	public Optional<BaseAnnotation> deserialize(String input, Class<? extends BaseAnnotation> clazz) {
		log.info("Deserializing {" + input + "}");

		Element rootElement = null;
		try {
			Document document = this.builder.parse(new InputSource(new StringReader(input)));
			rootElement = document.getDocumentElement();
		} catch (SAXException | IOException e) {
			log.error("Exception while parsing Annotation XML", e);
			return Optional.empty();
		}

		try {
			BaseAnnotation annotation = clazz.getConstructor().newInstance();

			Field valueField = annotation.getClass().getSuperclass().getDeclaredField("value");
			valueField.setAccessible(true);
			valueField.set(annotation, rootElement.getTextContent());

			NamedNodeMap namedNodeMap = rootElement.getAttributes();

			for (int i = 0; i < namedNodeMap.getLength(); i++) {
				Node node = namedNodeMap.item(i);
				if (StringUtils.isNotBlank(node.getNodeValue())) {
					Optional<Field> optionalField = getFieldByName(annotation, node.getNodeName());
					if(optionalField.isPresent()) {
						Field field = optionalField.get();
						field.setAccessible(true);
						field.set(annotation, node.getNodeValue());
					}
				}
			}
			return Optional.of(annotation);
		} catch (Exception e) {
			log.error("Error while Deserializing Annotation XML", e);
		}
		return Optional.empty();
	}

	private Optional<Field> getFieldByName(BaseAnnotation annotation, String fieldName) {
		try {
			return Optional.of(annotation.getClass().getDeclaredField(fieldName));
		} catch (NoSuchFieldException | SecurityException e) {
			return Optional.empty();
		}
	}
	
}
