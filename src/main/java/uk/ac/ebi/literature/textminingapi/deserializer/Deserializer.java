package uk.ac.ebi.literature.textminingapi.deserializer;

import java.util.Optional;

@FunctionalInterface
public interface Deserializer<T> {

	Optional<T> deserialize(String input, Class<? extends T> clazz);

}
