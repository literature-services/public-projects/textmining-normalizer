package uk.ac.ebi.literature.textminingapi.pojo;

/**
 * <z:go ids="GO:0007612" onto="biological_process">learning</z:go>
 * 
 * @author ssharma
 *
 */
public class GeneOntology extends BaseAnnotation {

	private String ids;
	private String onto;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getOnto() {
		return onto;
	}

	public void setOnto(String onto) {
		this.onto = onto;
	}

	@Override
	public String getType() {
		return "Gene Ontology";
	}

	@Override
	public String getUrl() {
		return new StringBuilder("http://identifiers.org/go/").append(getId(this.ids)).toString();
	}

}
