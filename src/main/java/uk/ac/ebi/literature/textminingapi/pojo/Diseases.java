package uk.ac.ebi.literature.textminingapi.pojo;

/**
 * 
 * <z:disease ids="C0011847,C0011849" SemGrp="T047,T047">diabetes</z:disease>
 * 
 * @author ssharma
 *
 */
public class Diseases extends BaseAnnotation {

	private String ids;
	private String SemGrp;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getSemGrp() {
		return SemGrp;
	}

	public void setSemGrp(String semGrp) {
		SemGrp = semGrp;
	}

	@Override
	public String getType() {
		return "Diseases";
	}

	@Override
	public String getUrl() {
		return new StringBuilder("http://linkedlifedata.com/resource/umls-concept/").append(getId(this.ids)).toString();
	}

}
