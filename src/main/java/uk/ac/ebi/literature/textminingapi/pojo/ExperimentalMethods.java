package uk.ac.ebi.literature.textminingapi.pojo;

/**
 * 
 * <z:methods ids="EFO_0008929" src="efo">SMA</z:methods>
 * 
 * @author ssharma
 *
 */
public class ExperimentalMethods extends BaseAnnotation {

	private String ids;
	private String src;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	@Override
	public String getType() {
		return "Experimental Methods";
	}

	@Override
	public String getUrl() {
		if (ids.startsWith("EFO"))
			return new StringBuilder("http://www.ebi.ac.uk/efo/").append(getId(this.ids)).toString();
		else
			return new StringBuilder("http://purl.obolibrary.org/obo/").append(getId(this.ids)).toString();
	}

}
