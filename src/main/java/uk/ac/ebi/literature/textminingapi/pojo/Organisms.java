package uk.ac.ebi.literature.textminingapi.pojo;

/**
 * 
 * <z:species ids=801" classname="scientific_name">BEV</z:species>
 * 
 * @author ssharma
 *
 */
public class Organisms extends BaseAnnotation {

	private String ids;
	private String classname;

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getClassname() {
		return classname;
	}

	public void setClassname(String classname) {
		this.classname = classname;
	}

	@Override
	public String getType() {
		return "Organisms";
	}

	@Override
	public String getUrl() {
		return new StringBuilder("http://identifiers.org/taxonomy/").append(getId(this.ids)).toString();
	}

}
