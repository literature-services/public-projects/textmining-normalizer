package uk.ac.ebi.literature.textminingapi.pojo;

/**
 * <z:uniprot sup="inside" fb="151" ids=
 * "P01114,O42785,P0CQ43,P0CQ42,G0LD36,A0PDV5">RAS</z:uniprot>
 * 
 * @author ssharma
 *
 */
public class GeneProteins extends BaseAnnotation {

	private String sup;
	private String fb;
	private String ids;

	public String getSup() {
		return sup;
	}

	public void setSup(String sup) {
		this.sup = sup;
	}

	public String getFb() {
		return fb;
	}

	public void setFb(String fb) {
		this.fb = fb;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	@Override
	public String getType() {
		return "Gene Proteins";
	}

	/**
	 * <p>
	 * <b>GeneProtein</b> : Simple URL building using the id
	 * </p>
	 */
	@Override
	public String getUrl() {
		return new StringBuilder("http://purl.uniprot.org/uniprot/").append(getId(this.ids)).toString();
	}
}
