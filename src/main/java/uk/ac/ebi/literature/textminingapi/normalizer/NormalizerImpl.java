package uk.ac.ebi.literature.textminingapi.normalizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import uk.ac.ebi.literature.textminingapi.deserializer.Deserializer;
import uk.ac.ebi.literature.textminingapi.pojo.BaseAnnotation;

@Component
@Qualifier("baseNormalizer")
public class NormalizerImpl implements Normalizer {

	private static final Logger log = LoggerFactory.getLogger(NormalizerImpl.class);

	private Deserializer<BaseAnnotation> deserializer;

	@Autowired
	public NormalizerImpl(@Qualifier("annotationXMLDeserializer") Deserializer<BaseAnnotation> deserializer) {
		this.deserializer = deserializer;
	}

	public List<BaseAnnotation> normalize(String content, String patternStr, Class<? extends BaseAnnotation> clazz) {
		log.info("Normalize Text File for {" + clazz.getSimpleName() + "}");
		Pattern pattern = Pattern.compile(patternStr);
		Matcher m = pattern.matcher(content);
		List<BaseAnnotation> annotations = new LinkedList<>();
		while (m.find()) {
			Optional<BaseAnnotation> optionalAnnotation = deserializer.deserialize(m.group(), clazz);
			if (optionalAnnotation.isPresent())
				annotations.add(optionalAnnotation.get());
		}
		log.info("After Text File Normalizing for {" + clazz.getSimpleName() + "}, found {" + annotations.size()
				+ "} Annotations");
		return annotations;
	}

}
